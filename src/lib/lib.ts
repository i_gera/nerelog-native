import * as appsData from 'Assets/json/NeRelog_apps.json';
import * as clientsData from 'Assets/json/NeRelog_clients.json';

export const device = {
    up: {
        xs: '(min-width: 375px)',
        sm: '(min-width: 768px)',
        md: '(min-width: 992px)',
        lg: '(min-width: 1200px)',
        xl: '(min-width: 1440px)'
    }
};

export function pxToRem(px: number) {
    return `${px / 16}rem`;
}

export function mergeArrByKey(arr1, arr2, findCallback) {
    return arr1.map((item1) => ({
        ...arr2.find((item2) => findCallback(item1, item2)),
        ...item1
    }));
}