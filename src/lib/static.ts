import appsData from 'Assets/json/NeRelog_apps.json';
import appsBigData from 'Assets/json/NeRelog_apps_5000.json';
import clientsData from 'Assets/json/NeRelog_clients.json';
import { mergeArrByKey } from './lib';

export const jsonData = mergeArrByKey(appsData, clientsData, (item1, item2) => item1.client_id === item2.id)

export const getData = () => {
    let test = [];
    jsonData.forEach(function(order) {
        test.push(JSON.parse('{"type": "Feature",  "properties": { "orderId": "'+order.id+'", "category": "'+order.type+'" }, "geometry": {"type": "Point", "coordinates": ['+order.coords.long+','+order.coords.lat+']}}'));
    });
    return test;
} 

export const getSourceData = () => {
    let test: any = [];
    appsBigData.forEach(function(order) {
        test.push({
            type: "Feature",  
            properties: { orderId: order.id, category: order.type }, 
            geometry: {
                type: "Point", 
                coordinates: [order.coords.long, order.coords.lat]}});
    });

    const src = {
        type: "FeatureCollection",
        features: test
    }
    return src;
} 

interface FeatureType {
    
}

type FeatureCollectionType = GeoJSON.Feature<GeoJSON.Geometry> | GeoJSON.FeatureCollection<GeoJSON.Geometry> | string | undefined;