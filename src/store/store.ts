import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { rootReducer, AppStateType } from './root-reducer';
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';

export type InferActionsTypes<T> = T extends {
    [key: string]: (...args: any[]) => infer U;
}
    ? U
    : never;

//type for thunk
export type BaseThunkType<
    A extends Action,
    R = Promise<void> | void
> = ThunkAction<R, AppStateType, unknown, A>;

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;

export const store = configureStore({
    reducer: rootReducer,
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            immutableCheck: false,
            serializableCheck: false
        })
});


