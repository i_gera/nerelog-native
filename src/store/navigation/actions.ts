import { InferActionsTypes } from '../store';

export const NAVIGATION_SET_PAGE = 'NAVIGATION_SET_PAGE';

const actions = {
    setPage: (page: string) =>
        ({
            type: NAVIGATION_SET_PAGE,
            payload: page
        } as const)
};

export type NavigationActions = InferActionsTypes<typeof actions>;

export const navigationActions = {
    ...actions
};
