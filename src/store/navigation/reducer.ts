import { NavigationActions, NAVIGATION_SET_PAGE } from './actions';

interface InitialState {
    currentPage: string;
}

const initialState: InitialState = {
    currentPage: '/'
};

export const navigationReducer = (
    state = initialState,
    action: NavigationActions
): InitialState => {
    switch (action.type) {
        case NAVIGATION_SET_PAGE:
            return {
                ...state,
                currentPage: action.payload
            };
        default:
            return state;
    }
};
