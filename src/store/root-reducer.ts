import { mapReducer } from '../pages/model';
import { combineReducers } from 'redux';
import { navigationReducer } from './navigation';

export const rootReducer = combineReducers({
    navigation: navigationReducer,
    map: mapReducer
});

type rootReducerType = typeof rootReducer;
export interface AppStateType extends ReturnType<rootReducerType> {}
