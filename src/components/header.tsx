import { media, pxToRem } from 'Lib';
import React, { useEffect } from 'react';
import styled from 'styled-components';
import { Link, useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { AppStateType } from 'Store/root-reducer';
import { navigationActions } from 'Store/navigation';

const { setPage } = navigationActions;

export const Header = () => {

    const currentPage = useSelector(
        ({ navigation: { currentPage } }: AppStateType) => currentPage
    );
    const location = useLocation();
    const dispatch = useDispatch();

    useEffect(() => {
        const { pathname } = location;
        dispatch(setPage(pathname));
    }, [location, dispatch]);

    const isActive = (to: string) =>
        to.split('/')[1] === currentPage.split('/')[1];
 
    return (
        <StyledContainer>
            <StyledLogo>NeRelog</StyledLogo>
            <StyledNav>
            <StyledLink
                to="/"
                $isCurrentPage={isActive("/")}
                >
                Основная
            </StyledLink>
            <StyledLink
                to="/additional"
                $isCurrentPage={isActive("/additional")}
                >
                Дополнительно
            </StyledLink>
            </StyledNav>
        </StyledContainer>
    );
};

const StyledContainer = styled.div`
    width: 100%;
    background-color: #860bd7;
    height: ${pxToRem(60)};
    border: none;
    border: 1px red;
    display: flex;
    justify-content: space-between;
`;

const StyledLogo = styled.div`
    font-size: ${pxToRem(36)};
    color: #fff;
    padding: ${pxToRem(10)} ${pxToRem(24)};
`;

const StyledNav = styled.div`
    display: flex; 
    /* justify-content: space-between; */
    align-items: center;
    padding-right: ${pxToRem(20)};
`;

interface StyledProps {
    $isCurrentPage?: boolean;
}

const StyledLink = styled(Link)<StyledProps>`
    text-decoration: none;
    font-size: ${pxToRem(16)};
    color: ${({ $isCurrentPage }) => ($isCurrentPage ? '#ffba03' : '#fff')};
    font-weight: ${({ $isCurrentPage }) => $isCurrentPage && 700};
    text-align: center;
    padding: ${pxToRem(10)};
    &:not(first-child) {
        margin-right: 1px;
    }

    ${media.sm} {
        display: ${({ $isCurrentPage }) => ($isCurrentPage ? 'none' : 'block')};
    }
    
`