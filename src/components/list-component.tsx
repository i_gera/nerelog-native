import React from 'react';
import styled from 'styled-components'; 
import { FixedSizeList } from 'react-window';
import { jsonData } from 'Lib/static';
import AutoSizer from "react-virtualized-auto-sizer";
import { ListItem } from './list-item';
import { pxToRem } from 'Lib';
 
const Row = ({ index, style }) => (
  <ListItem data={...jsonData[index] } style={style}/>
);
export const ListComponent = () => {
    return <AutoSizer>
    {({ height, width }) => (
        <FixedSizeList
            height={height}
            width={width}
            itemSize={160}
            itemCount={jsonData.length}
  >
    {Row}
  </FixedSizeList>
  )}
  </AutoSizer>
};