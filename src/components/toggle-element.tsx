import { pxToRem } from 'Lib';
import React from 'react';
import { FC } from 'react';
import styled from 'styled-components';
import { useAppDispatch, useAppSelector } from '../store';
import { mapActions } from 'Pages/model';

const {setIsList} = mapActions;

export const ToggleElement: FC<ComponentProps> = () => {
    const isList = useAppSelector(({ map: { isList } }) => isList);
    const dispatch = useAppDispatch();
    return (
        <StyledContainer>
            <StyledTypeContainer
                isChosen={isList}
                onClick={() => {
                    dispatch(setIsList(true));
                }}
            >
               Списком
            </StyledTypeContainer>
            <StyledTypeContainer
                isChosen={!isList}
                onClick={() => {
                    dispatch(setIsList(false));
                }}
            >
                На карте
            </StyledTypeContainer>
        </StyledContainer>
    );
};

interface ComponentProps {
    
    // isList: boolean;
    // setIsList: (boolean) => void;
}

interface StyledTypeContainerProps {
    isChosen: boolean;
}

const StyledContainer = styled.div`
    display: flex;
    width: 80%;
    justify-content: center;
    margin: 0 auto;
`;
const StyledTypeContainer = styled.div<StyledTypeContainerProps>`
    cursor: pointer;
    font-size: 16px;
    line-height: 19px;
    border: ${({ isChosen }) => !isChosen && '1px solid #fff'};
    &:first-child {
        border-radius: 4px 0px 0px 4px;
    }
    &:last-child {
        border-radius: 0px 4px 4px 0px;
    }
    padding: 4px 20px;
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    font-weight: ${({ isChosen }) => (isChosen ? 'bold' : 'normal')};
    color: ${({ isChosen }) => (isChosen ? '#860bd7' : '#fff')};
    background-color: ${({ isChosen }) => (isChosen ? '#fff' : 'transparent')};
`;
