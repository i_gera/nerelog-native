import React, { useState, useRef, useMemo, useEffect } from "react";
import {Map, Source, Layer, GeolocateControl, NavigationControl, ScaleControl} from 'react-map-gl';
import {clusterLayer, clusterCountLayer, unclusteredPointLayer} from './layers';
import type {MapRef} from 'react-map-gl';
import type {GeoJSONSource} from 'react-map-gl';
import styled from "styled-components";
import {getSourceData} from 'Lib/static';

const MAPBOX_TOKEN =
    'pk.eyJ1IjoiaS1nZXJhIiwiYSI6ImNsMDU1Y2I4YTB0angzZnFkNHlydnRld3gifQ.sa38mj69HptoxV9DHSwc6Q'; // Set your mapbox token here

export const MapComponentCluster = () => {
  const mapRef = useRef<MapRef>(null);
  const [data, setData] = useState(null);

  const d: any = getSourceData();

  useEffect(() => {
    setData(d);
  }, [])

  const onClick = event => {
    const feature = event.features[0];
    const clusterId = feature.properties.cluster_id;

    const mapboxSource = mapRef.current.getSource('orders') as GeoJSONSource;

    mapboxSource.getClusterExpansionZoom(clusterId, (err, zoom) => {
      if (err) {
        return;
      }

      mapRef.current.easeTo({
        center: feature.geometry.coordinates,
        zoom,
        duration: 500
      });
    });
  };
  return (
    <>
      <Map
         initialViewState={{
          latitude: 43.238949,
          longitude: 76.889709,
          zoom: 12,
          bearing: 0,
          pitch: 0
      }}
        style={{ width: '100%', height: '100%' }}
        mapStyle="mapbox://styles/mapbox/streets-v9"
        mapboxAccessToken={MAPBOX_TOKEN}
        interactiveLayerIds={[clusterLayer.id]}
        onClick={onClick}
        ref={mapRef}
      >
        <GeolocateControl position="top-left" />
        <NavigationControl position="top-left" />
        <ScaleControl />
        <Source
          id="orders"
          type="geojson"
          data={data}
          cluster={true}
          clusterMaxZoom={14}
          clusterRadius={50}
        >
          <Layer {...clusterLayer} />
          <Layer {...clusterCountLayer} />
          <Layer {...unclusteredPointLayer} />
        </Source>
      </Map>
    </>
  );
}

const StyledMarker = styled.div` 
color: #fff;
background: black;
border-radius: 50%;
padding: 10px;
display: flex;
justify-content: center;
align-items: center;`

const StyledSingleMarker = styled.button` 
color: #fff;
background: transparent;
border-radius: 50%;
padding: 10px;
display: flex;
justify-content: center;
align-items: center;`