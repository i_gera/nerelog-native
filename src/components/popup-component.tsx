import React from 'react';
import { Popup } from 'react-map-gl';
import styled from 'styled-components';

export const PopupComponent = ({ long, lat }) => {
    const [showPopup, setShowPopup] = React.useState(true);

    return (
        <>
            {showPopup && (
                <Popup
                    longitude={long}
                    latitude={lat}
                    anchor="bottom"
                    onClose={() => setShowPopup(false)}
                >
                    You are here
                </Popup>
            )}
        </>
    );
};
