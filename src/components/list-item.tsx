import { pxToRem } from 'Lib';
import { Card } from './card';
import React from 'react';
import styled from 'styled-components';

export const ListItem = (props) => {
    const { price, name, type, phone } = props.data;
    return (
        <StyledContainer style={props.style}>
            <Card>
                <StyledType typeDelivery={type === 'delivery'}>
                    Тип: {type === 'delivery' ? 'Доставка' : 'Забор'}
                </StyledType>
                <StyledPrice>Цена: {price.toFixed(2)} KZT</StyledPrice>
                <StyledClient>Компания: {name}</StyledClient>
                <StyledPhone>Телефон: {phone}</StyledPhone>
            </Card>
        </StyledContainer>
    );
};

const StyledContainer = styled.div`
    margin-top: ${pxToRem(10)};
    padding: 0 ${pxToRem(10)};
`;

interface StyledTypeProps {
    typeDelivery?: boolean;
}

const StyledType = styled.div<StyledTypeProps>`
    font-size: ${pxToRem(14)};
    font-weight: 600;
    color: ${({ typeDelivery }) => (!typeDelivery ? '#ffba03' : '#860bd7')};
`;

const StyledPrice = styled.div`
    font-size: ${pxToRem(12)};
    color: #00dd51;
    padding-top: ${pxToRem(10)};
`;

const StyledClient = styled.div`
    font-size: ${pxToRem(14)};
    padding-top: ${pxToRem(10)};
`;

const StyledPhone = styled.div`
    font-size: ${pxToRem(12)};
    padding-top: ${pxToRem(10)};
    color: #999;
`;
