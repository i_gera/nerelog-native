import { device, pxToRem } from 'Lib';
import React, { FC } from 'react';
import styled, { FlattenSimpleInterpolation } from 'styled-components';

export const Card: FC<ComponentProps> = ({ $style, title, children }) => {
    return (
        <StyledContainer $style={$style}>
            {title && <StyledTitle>{title}</StyledTitle>}
            <StyledContent>{children}</StyledContent>
        </StyledContainer>
    );
};

interface ComponentProps {
    title?: string;
    $style?: FlattenSimpleInterpolation;
}

interface StyledContainerProps {
    $style?: FlattenSimpleInterpolation;
}

const StyledContainer = styled.div<StyledContainerProps>`
    background: #fff;
    box-shadow: 0px 2px 5px rgba(71, 53, 96, 0.05);
    border-radius: ${pxToRem(5)};
    padding: ${pxToRem(24)};
    ${({ $style }) => $style};
`;

const StyledTitle = styled.div`
    font-size: ${pxToRem(24)};
    @media ${device.up.sm} {
        font-size: ${pxToRem(36)};
    }
`;

const StyledContent = styled.div`
    margin-top: ${pxToRem(16)};
    width: 100%;
`;
