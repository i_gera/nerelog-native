import React, { useMemo, useState } from 'react';
import styled from 'styled-components';
import Map, {
    FullscreenControl,
    GeolocateControl,
    Marker,
    NavigationControl,
    Popup,
    ScaleControl
} from 'react-map-gl';
import { jsonData } from 'Lib/static';
import Pin from './pin';

const MAPBOX_TOKEN =
    'pk.eyJ1IjoiaS1nZXJhIiwiYSI6ImNsMDU1Y2I4YTB0angzZnFkNHlydnRld3gifQ.sa38mj69HptoxV9DHSwc6Q'; // Set your mapbox token here
export const MapComponent = () => {
    const [hoverInfo, setHoverInfo] = useState(null);

    const pins = useMemo(
        () =>
            jsonData.map((item, idx) => {
                const { coords, type } = item;
                return (
                    <Marker
                        key={`marker-${idx}`}
                        longitude={coords.long}
                        latitude={coords.lat}
                        anchor="bottom"
                    >
                        <Pin
                            type={type}
                            onMouseOver={() => {
                                setHoverInfo(item);
                            }}
                            onMouseOut={() => {
                                setHoverInfo(null);
                            }}
                        />
                    </Marker>
                );
            }),
        []
    );

    return (
        <Map
            initialViewState={{
                latitude: 43.238949,
                longitude: 76.889709,
                zoom: 14,
                bearing: 0,
                pitch: 0
            }}
            style={{ width: '100%', height: '100%' }}
            mapStyle="mapbox://styles/mapbox/streets-v9"
            mapboxAccessToken={MAPBOX_TOKEN}
        >
            <GeolocateControl position="top-left" />
            <NavigationControl position="top-left" />
            <ScaleControl />

            {pins}
            {hoverInfo && (
                <Popup
                    latitude={hoverInfo.coords.lat}
                    longitude={hoverInfo.coords.long}
                    onClose={() => {
                        setHoverInfo(null);
                    }}
                    anchor="top"
                    closeButton={false}
                >
                    <div>ID заказа: {hoverInfo.id}</div>
                    <div>Клиент: {hoverInfo.name}</div>
                    <div>Цена: {hoverInfo.price.toFixed(2)} KZT</div>
                </Popup>
            )}
        </Map>
    );
};
