import MainScreen from 'Pages/main';
import AdditionalScreen from 'Pages/additional';
import React, { lazy, Suspense } from 'react';
import { Switch, Route } from 'react-router-dom';

export const Router = () => {
    return (
        <Suspense fallback={null}>
            <Switch>
                <Route path="/additional" component={AdditionalScreen} />
                <Route path="/" component={MainScreen} />
            </Switch>
        </Suspense>
    );
};
