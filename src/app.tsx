import { GlobalStyles } from './global-styles';
import React, { Suspense } from 'react';
import styled from 'styled-components';
import MainScreen from './pages/main';
import { DetectDeviceProvider } from './detect-device';
import { BrowserRouter } from 'react-router-dom';
import { Router } from './router'; 
import { Provider } from 'react-redux';
import { store } from './store';
export const App = ({ name = "text" }: { name?: string }) => (
    <Suspense fallback={null}>
        <Provider store={store}>
            <GlobalStyles />
            <DetectDeviceProvider>
                <BrowserRouter basename="/">
                    <Router />
                </BrowserRouter>
            </DetectDeviceProvider>
            </Provider>
</Suspense>
);

export const StyledContainer = styled.div`
`;