import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface InitialState {
    isList: boolean;
}

const initialState: InitialState = {
    isList: true
};

export const {
    actions: mapActions,
    reducer: mapReducer
} = createSlice({
    name: 'map',
    initialState,
    reducers: {
        setIsList(state, { payload }: PayloadAction<boolean>) {
            state.isList = payload
        }
    }
});