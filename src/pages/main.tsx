import { Header } from 'Components/header';
import { media, pxToRem } from 'Lib';
import React, { memo } from 'react';
import styled from 'styled-components';
import { MapComponent } from 'Components/map-component';
import {ListComponent} from 'Components/list-component';
import { ToggleElement } from 'Components/toggle-element';
import { useAppSelector } from 'Store';

const _MainScreen = () => {
    const isList = useAppSelector(({ map: { isList } }) => isList);
    return (
        <StyledContainer>
            <Header />
            <StyledToggle>
                <ToggleElement />
            </StyledToggle>
            <StyledContent>
                <StyledList isList={isList}>
                    <StyledTitle>Список заявок</StyledTitle>
                    <ListComponent />
                </StyledList>
                <StyledMap >
                    <MapComponent />
                </StyledMap>
            </StyledContent>
        </StyledContainer>
    );
};

const MainScreen = memo(_MainScreen);
export default MainScreen;

const StyledContainer = styled.div`
    width: 100vw;
    height: 100vh;
`;

const StyledToggle = styled.div`
   background-color: #860bd7;
   padding: ${pxToRem(10)} 0;
   display: none;
   ${media.sm} {
        display: block;
    }
`;

const StyledContent = styled.div`
position: relative;
    display: flex;
    justify-content: space-between;
    ${media.sm} {
        display: block;
    }
`;
const StyledTitle = styled.div`
    font-size: ${pxToRem(18)};
    padding: ${pxToRem(20)};
    padding-bottom: ${pxToRem(10)};
`;

interface StyledProps {
    isList: boolean;
}
const StyledList = styled.div<StyledProps>`
position: relative;
    /* width: 30%; */
    min-width: ${pxToRem(350)};
    height: calc(100vh - 140px);
    z-index: 100;
    background-color: transparent;

    ${media.sm} {
        position: absolute;
        top:0;
        left: 0;
        width: 100%;
        display: ${({isList}) => isList ? 'block' : 'none' };
        background-color: #f8f8f8;
        height: 100vh;

    }
`;
const StyledMap = styled.div`
    position: relative;
    width: 100%;
    height: calc(100vh - 60px);
    ${media.sm} {
        width: 100%;
    }
`;
