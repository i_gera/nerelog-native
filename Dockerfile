FROM node:14-alpine 
WORKDIR /app
COPY package*.json ./
RUN yarn
COPY . . 
EXPOSE 3000
RUN yarn build
RUN yarn global add serve
CMD serve -s dist 